import $ from 'jquery';
import '@chenfengyuan/datepicker';

$(document).ready(function(){

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    
    let fakeAjax = number => {
        let time = getRandomInt(1,10) * 1000;
        return new Promise((resolve,reject) => {
            setTimeout(() => {
                resolve(number);
            },time);
        });
    }
    
    function * generator(){
        // yield getValue(getRandomInt(2,10));
        // yield getValue(getRandomInt(2,10));
        // yield getValue(getRandomInt(2,10));
    }
    
    let iterator = generator();
    
    // console.log(iterator.next());
    // console.log(iterator.next());
    
    
    let promise1 = fakeAjax(1);
    let promise2 = fakeAjax(2);
    let promise3 = fakeAjax(3);
    let promise4 = fakeAjax(4);
    let promise5 = fakeAjax(5);
    let promise6 = fakeAjax(6);
    
    $('[data-toggle="datepicker"]').datepicker();
    $().datepicker({
        date: new Date() // Or '02/14/2014'
      });
    });

    

    $().on('show.datepicker', function (e) {
        console.log(e);
        if (e.date < new Date()) {
          e.preventDefault(); // Prevent to pick the date
          console.log("Żle!!!");
        }
    });